# Virtual Machines

## SENATUS with SDN Mininet testbed (SENATUS_SDN.ova)
The SENATUS_SDN.ova is a virtual machine to be run with Oracle VirtualBox. It is preconfigured to use 4 CPUs and 10 GB of RAM. 
In additions, it has two virtual network connectors: one configured as a NAT and another as bridge. Parameters of the container 
can be change maintaining a minimum of 6 GB of RAM for running ONOS and Mininet. Virtual NIC can be remove is external connectivity if no necessary.

### Starting the environment
Login credential of the operating system are:
* User: sdn
* Password: rocks

For starting ONOS, there is a script already prepared on the desktop called “Setup ONOS Cluster”. This script deploys a cluster of 3 Docker containers 
that works as the SDN controller. One of the container of the cluster takes the role of master, while the others are ready to step in case the master falls.
Moreover, is case of misconfiguration or a persistent error from the cluster, rerunning the script will remove the old ONOS and start a fresh one. 
Once ONOS is running, we can launch Mininet with the BONSAI topology. For launching the topology, we have another script that automatizes the process: “BONSAI lab Topology”. 
Launching this script will start Mininet and create a topology of 12 OpenFlow switches (s1, s2, s3……s12) with 4 host (h1, h6, h7, h12).
The number from the labels of the host correspond to the switch to which the host is connected.

Once Mininet is running, we can access to the ONOS GUI using the script on the desktop “ONOS GUI” or using the browser on the URL 172.17.0.2:8181/onos/ui/login.html (Using IP 127.0.0.1 is also valid). 
Credentials are:
* User: onos
* Password: rocks

If it is the first time ONOS is started on the current session, we need to activate some of the ONOS native apps to discover the topology. 
In the options bar, go to application section. Look for the application OpenFlow Provider Suite and active the app. This application will automatically start the set basic applications. 
In addition, we can start Reactive Forwarding application if we want ONOS to install automatically rules for the traffic. 
These rules have a life time of 10 seconds and they will be automatically removed if the flow rule is not being used or the app is stopped. 
While Reactive Forwarding is not used during the experiments, is useful to check that the topology and host are correctly deployed.

### Generating traffic

Once ONOS is running and the topology has been created, we can start generating some traffic. Let’s assume that we are using ONOS Reactive Forwarding or that the flow rules are already created.
To access one of the host of Mininet (for instance h1), go to the terminal where Mininet is running a run the command “h1 xterm &”. 
This command will open a terminal on h1 without blocking the Mininel CLI. Inside h1 we can generate some ping to another host (Eg: h6) and, if rules are installed, see that we have the replay.
 
The IP and MAC address of the host on the topology are as follows (HOST, MAC, IP):
* h1	00:00:00:00:00:01	10.0.0.1
* h2	00:00:00:00:00:02	10.0.0.2
* h3	00:00:00:00:00:03	10.0.0.3
* h4	00:00:00:00:00:04	10.0.0.4

When a host start generating traffic, will be discovered automatically by ONOS. Press “h” on topology view to show hosts discovered. 
Pressing “l” the label with the id from the devices and the MAC address from the hosts appear. SHIFT+h will change MAC address view by IP address.

Inside each host, there is installing the Distributed Internet Traffic Generator tool on path /home/sdn/D-ITG-2.8.1-r1023/. 
This tools can be used to generate advance traffic profiles between the hosts. Inside the folder of this tools, on bin/ there are several scripts already prepare 
to generate traffic between all the hosts. To generate the traffic, first we have to setup a TCP server on each host. 
Open one terminal for each host (using xterm command), and run ./bin/ITGRecv for each one of them. 
Then, we can initiate the clients open new terminals on the hosts and using ITGSend command. 
This command need to be specify the profile of the traffic to generate. On the same folder there are four 
scripts that already contain the profiles to generate TCP traffic to the other three hosts. Just run inside bin folder, for instance ./ITGSend script_file1 for h1 and so. 
The scripts are configured for running traffic for the period of time specified on parameter -t. 
By default, this time are several hours. With the four servers and four clients running with the scripts, 
we have the topology populated. On the topology view, if pressing “a” several times, you can see different 
views of the traffic on the network.

### Running SENATUS

(coming soon)



 





