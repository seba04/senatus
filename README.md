# SENATUS

SENATUS is an experimental service orchestrator targeting research and testing environments. 
SENATUS implements a set of innovations to support the validation of the future network 
planner modules that will be integrated on the management entities of the 5G architecture.

More information can be found in the following paper presented at Fourth IEEE International Workshop on
Orchestration for Software Defined Infrastructures held in conjunction with the 4th IEEE 
Conference on Network Function Virtualization and Software Defined Networks (NFV-SDN 2018).

Before running SENATUS, you need to fulfill the following requirements:

## OS
Ubuntu 16.04 LTS updated. 

## Softwares
* CPLEX
* Net2Plan
* Mininet
* D-ITG
* ONOS 1.12 Magpie
* Openstack

## Hardware
2 PC (ONOS+SENATUS and Openstack) with at least:
* 4CPU
* 32 GB RAM

4 PC (compute nodes) with at least:
* 2 CPU
* 8 GB RAM
